#!/bin/sh
#
# Update local repository if necessary and (re-)run install-script

path=$(dirname $(readlink -f "$0"))

# fetch metadata
git -C $path fetch

# compare local head and remote master
if [ "`git -C $path rev-parse @`" != "`git -C $path rev-parse origin/master`" ]; then
	git -C $path pull
	$path/doinit-install.sh
fi



