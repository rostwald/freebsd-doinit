#!/bin/sh
#
# Initial cleanup and configuration for new droplets that will be autoconfigured
# by freebsd-doinit.
# This script is intended to be run by the pre-installed cloudinit via user-data
# or manually on a freshly deployed droplet originating from a default DO-image.
# Don't run this script on an already modified system as it might have undesired
# side-effects on existing configurations (esp. rc.conf).

# get path
path=$(dirname $(readlink -f "$0"))

. $path/doinit.conf

# check if we need to initialize the droplet or exit gracefully
grep -c "# DigitalOcean Dynamic Configuration" /etc/rc.conf || exit 0

# we want a clean base system, so remove all packages except pkg
pkg delete -yqa
pkg clear

# cleanup cloudinit and DO-specific configs
rm -f /usr/local/etc/rc.d/digitalocean*
rm -f /usr/local/etc/rc.conf.d/digitalocean.conf
rm -rf /usr/local/etc/cloud

# remove extra user created on DO-droplets
rmuser -y freebsd

# cleanup rc.conf
sed -i '' -e '/hostname/d' -e '/cloudinit/d' -e '/digitalocean/d' -e '/DigitalOcean Dynamic Configuration/,$d' /etc/rc.conf

# run installation of freebsd-doinit
localrepo=$(fetch -q -o - $metadata_url/user-data | awk '/git clone/ { print $(NF)}')
sh $path/doinit-install.sh
