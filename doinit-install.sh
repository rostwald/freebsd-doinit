#!/bin/sh
#
# Install freebsd-doinit scripts and configuration to system; update doinit.
# This script installs and configures freebsd-doinit to a clean base system,
# either after manual OS-install or after cleanup of a DigitalOcean droplet
# by the 'init.sh' script. I
# 'init.sh' runs this script after successful cleanup; 'update.sh' after 
# successfull updates. It is safe to run this script manually even on already 
# configured systems.

configfile="/usr/local/etc/doinit.conf"
path=$(dirname $(readlink -f "$0"))

# source the configfile or get default config from same directory
logger "# source the configfile or get default config from same directory"
if [ -f $configfile ]; then
	. $configfile  
else
	. $path/doinit.conf
fi

# override empty (default) settings with values from user-data
logger "# override empty (default) settings with values from user-data"
localrepo=${localrepo:-$(fetch -q -o - $metadata_url/user-data | awk '/git clone/ { print $(NF)}')}
doinit_auto_update=${doinit_auto_update:-$(fetch -q -o - $metadata_url/user-data | grep 'doinit_auto_update' | cut -d= -f2)}
# if doinit_auto_update was not set in user-data, assume "no"
doinit_auto_update=${doinit_auto_update:-no}

# if autoupdates are enabled make sure git is avaliable and create a cronjob
logger "# if autoupdates are enabled make sure git is avaliable and create a cronjob"
if [ $doinit_auto_update == "yes" ]; then
	pkg install -yq git-lite
	[ -z $( grep "doinit-update.sh" /etc/crontab ) ] && \
	cat >> /etc/crontab << EOF
#
# update doinit
0 6 * * *	root	$localrepo/doinit-update.sh
EOF

fi

# make sure target directories are present
logger "# make sure target directories are present"
install -d /usr/local/etc/rc.d
install -d /usr/local/sbin

install -m 0700 $localrepo/doinit /usr/local/etc/rc.d/
install -m 0700 $localrepo/doinit.sh /usr/local/sbin/

# always re-install the default config file and populate it with the previously gathered values; this enables easy updates to the config file
logger "#always re-install the default config file and populate it with the previously gathered values; this enables easy updates to the config file"
install -m 0644 $localrepo/doinit.conf $configfile
sed -i '' "s#localrepo=#localrepo=\"$localrepo\"#" $configfile
sed -i '' "s#doinit_auto_update=#doinit_auto_update=$doinit_auto_update#" $configfile

# enable the service
logger "# enable the service"
if [ ! $(grep "doinit_enable=" /etc/rc.conf) ] && [ ! $(grep "doinit_enable=" /etc/rc.conf.local) ]; then
	[ -f /etc/rc.conf.local ] && echo "doinit_enable=YES" >> /etc/rc.conf.local || echo "doinit_enable=YES" >> /etc/rc.conf
fi
